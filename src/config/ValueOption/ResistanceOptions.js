// 耐性
export function getResistanceOptions () {
  return [
    {
      'value': '+2.0%',
      'text': '+2.0%'
    },
    {
      'value': '+4.0%',
      'text': '+4.0%'
    },
    {
      'value': '+6.0%',
      'text': '+6.0%'
    },
    {
      'value': '+8.0%',
      'text': '+8.0%'
    },
    {
      'value': '+10.0%',
      'text': '+10.0%'
    },
    {
      'value': '+12.0%',
      'text': '+12.0%'
    },
    {
      'value': '+14.0%',
      'text': '+14.0%'
    }
  ]
}
