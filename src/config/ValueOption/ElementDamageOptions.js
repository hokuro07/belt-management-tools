// 戦神のベルトの属性ダメージ
function getSenshinElementDamageOptions () {
  return [
    {
      'value': '+10%',
      'text': '+10%'
    },
    {
      'value': '+11%',
      'text': '+11%'
    },
    {
      'value': '+12%',
      'text': '+12%'
    },
    {
      'value': '+13%',
      'text': '+13%'
    }
  ]
}

// 輝石のベルトの属性ダメージ
function getKisekiElementDamageOptions () {
  return [
    {
      'value': '+4.0%',
      'text': '+4.0%'
    },
    {
      'value': '+5.0%',
      'text': '+5.0%'
    },
    {
      'value': '+6.0%',
      'text': '+6.0%'
    },
    {
      'value': '+8.0%',
      'text': '+8.0%'
    },
    {
      'value': '+10.0%',
      'text': '+10.0%'
    },
    {
      'value': '+11.0%',
      'text': '+11.0%'
    },
    {
      'value': '+12.0%',
      'text': '+12.0%'
    }
  ]
}

export {
  getSenshinElementDamageOptions,
  getKisekiElementDamageOptions
}
