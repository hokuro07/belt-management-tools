// 付与効果
export function getGrantAbilityOptions () {
  return [
    {
      'value': '2.0%',
      'text': '2.0%'
    },
    {
      'value': '2.5%',
      'text': '2.5%'
    },
    {
      'value': '3.0%',
      'text': '3.0%'
    },
    {
      'value': '3.5%',
      'text': '3.5%'
    },
    {
      'value': '4.0%',
      'text': '4.0%'
    },
    {
      'value': '4.5%',
      'text': '4.5%'
    },
    {
      'value': '5.0%',
      'text': '5.0%'
    }
  ]
}
