// 戦神のベルト 種族ダメージ
function getSenshinTypeDamageOptions () {
  return [
    {
      'value': '+6.0%',
      'text': '+6.0%'
    },
    {
      'value': '+7.0%',
      'text': '+7.0%'
    },
    {
      'value': '+8.0%',
      'text': '+8.0%'
    },
    {
      'value': '+9.0%',
      'text': '+9.0%'
    }
  ]
}

// 輝石のベルト 種族ダメージ
function getKisekiTypeDamageOptions () {
  return [
    {
      'value': '+4.0%',
      'text': '+4.0%'
    },
    {
      'value': '+5.0%',
      'text': '+5.0%'
    },
    {
      'value': '+6.0%',
      'text': '+6.0%'
    },
    {
      'value': '+7.0%',
      'text': '+7.0%'
    },
    {
      'value': '+8.0%',
      'text': '+8.0%'
    },
    {
      'value': '+9.0%',
      'text': '+9.0%'
    },
    {
      'value': '+10.0%',
      'text': '+10.0%'
    }
  ]
}

export {
  getSenshinTypeDamageOptions,
  getKisekiTypeDamageOptions
}
