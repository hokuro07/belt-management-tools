import { getSenshinElementDamageOptions, getKisekiElementDamageOptions } from './ValueOption/ElementDamageOptions.js'
import { getSenshinTypeDamageOptions, getKisekiTypeDamageOptions } from './ValueOption/TypeDamageOptions.js'
import { getResistanceOptions } from './ValueOption/ResistanceOptions.js'
import { getGrantAbilityOptions } from './ValueOption/GrantAbilityOptions.js'

export function getAbilityValueOptions () {
  // 属性
  const senshinElementDamageOptions = getSenshinElementDamageOptions()
  const kisekiElementDamageOptions = getKisekiElementDamageOptions()
  // 系統
  const senshinTypeDamageOptions = getSenshinTypeDamageOptions()
  const kisekiTypeDamageOptions = getKisekiTypeDamageOptions()
  // 耐性
  const resistanceOptions = getResistanceOptions()
  // 付与効果
  const grantAbilityOptions = getGrantAbilityOptions()

  return {
    '戦神のベルト': {
      'こうげき力': [
        {
          'value': '+10',
          'text': '+10'
        },
        {
          'value': '+12',
          'text': '+12'
        },
        {
          'value': '+14',
          'text': '+14'
        }
      ],
      'こうげき魔力とかいふく魔力': [
        {
          'value': '+16',
          'text': '+16'
        },
        {
          'value': '+20',
          'text': '+20'
        },
        {
          'value': '+24',
          'text': '+24'
        }
      ],
      '会心率と呪文暴走率': [
        {
          'value': '+1.5%',
          'text': '+1.5%'
        },
        {
          'value': '+2.0%',
          'text': '+2.0%'
        }
      ],
      '片手剣 - 炎属性ダメージ': senshinElementDamageOptions,
      '片手剣 - 氷属性ダメージ': senshinElementDamageOptions,
      '片手剣 - 風属性ダメージ': senshinElementDamageOptions,
      '片手剣 - 雷属性ダメージ': senshinElementDamageOptions,
      '片手剣 - 闇属性ダメージ': senshinElementDamageOptions,
      '片手剣 - 土属性ダメージ': senshinElementDamageOptions,
      '片手剣 - 光属性ダメージ': senshinElementDamageOptions,
      '両手剣 - 炎属性ダメージ': senshinElementDamageOptions,
      '両手剣 - 氷属性ダメージ': senshinElementDamageOptions,
      '両手剣 - 風属性ダメージ': senshinElementDamageOptions,
      '両手剣 - 雷属性ダメージ': senshinElementDamageOptions,
      '両手剣 - 闇属性ダメージ': senshinElementDamageOptions,
      '両手剣 - 土属性ダメージ': senshinElementDamageOptions,
      '両手剣 - 光属性ダメージ': senshinElementDamageOptions,
      '短剣 - 炎属性ダメージ': senshinElementDamageOptions,
      '短剣 - 氷属性ダメージ': senshinElementDamageOptions,
      '短剣 - 風属性ダメージ': senshinElementDamageOptions,
      '短剣 - 雷属性ダメージ': senshinElementDamageOptions,
      '短剣 - 闇属性ダメージ': senshinElementDamageOptions,
      '短剣 - 土属性ダメージ': senshinElementDamageOptions,
      '短剣 - 光属性ダメージ': senshinElementDamageOptions,
      'スティック - 炎属性ダメージ': senshinElementDamageOptions,
      'スティック - 氷属性ダメージ': senshinElementDamageOptions,
      'スティック - 風属性ダメージ': senshinElementDamageOptions,
      'スティック - 雷属性ダメージ': senshinElementDamageOptions,
      'スティック - 闇属性ダメージ': senshinElementDamageOptions,
      'スティック - 土属性ダメージ': senshinElementDamageOptions,
      'スティック - 光属性ダメージ': senshinElementDamageOptions,
      '両手杖 - 炎属性ダメージ': senshinElementDamageOptions,
      '両手杖 - 氷属性ダメージ': senshinElementDamageOptions,
      '両手杖 - 風属性ダメージ': senshinElementDamageOptions,
      '両手杖 - 雷属性ダメージ': senshinElementDamageOptions,
      '両手杖 - 闇属性ダメージ': senshinElementDamageOptions,
      '両手杖 - 土属性ダメージ': senshinElementDamageOptions,
      '両手杖 - 光属性ダメージ': senshinElementDamageOptions,
      'ヤリ - 炎属性ダメージ': senshinElementDamageOptions,
      'ヤリ - 氷属性ダメージ': senshinElementDamageOptions,
      'ヤリ - 風属性ダメージ': senshinElementDamageOptions,
      'ヤリ - 雷属性ダメージ': senshinElementDamageOptions,
      'ヤリ - 闇属性ダメージ': senshinElementDamageOptions,
      'ヤリ - 土属性ダメージ': senshinElementDamageOptions,
      'ヤリ - 光属性ダメージ': senshinElementDamageOptions,
      'オノ - 炎属性ダメージ': senshinElementDamageOptions,
      'オノ - 氷属性ダメージ': senshinElementDamageOptions,
      'オノ - 風属性ダメージ': senshinElementDamageOptions,
      'オノ - 雷属性ダメージ': senshinElementDamageOptions,
      'オノ - 闇属性ダメージ': senshinElementDamageOptions,
      'オノ - 土属性ダメージ': senshinElementDamageOptions,
      'オノ - 光属性ダメージ': senshinElementDamageOptions,
      'ハンマー - 炎属性ダメージ': senshinElementDamageOptions,
      'ハンマー - 氷属性ダメージ': senshinElementDamageOptions,
      'ハンマー - 風属性ダメージ': senshinElementDamageOptions,
      'ハンマー - 雷属性ダメージ': senshinElementDamageOptions,
      'ハンマー - 闇属性ダメージ': senshinElementDamageOptions,
      'ハンマー - 土属性ダメージ': senshinElementDamageOptions,
      'ハンマー - 光属性ダメージ': senshinElementDamageOptions,
      '棍 - 炎属性ダメージ': senshinElementDamageOptions,
      '棍 - 氷属性ダメージ': senshinElementDamageOptions,
      '棍 - 風属性ダメージ': senshinElementDamageOptions,
      '棍 - 雷属性ダメージ': senshinElementDamageOptions,
      '棍 - 闇属性ダメージ': senshinElementDamageOptions,
      '棍 - 土属性ダメージ': senshinElementDamageOptions,
      '棍 - 光属性ダメージ': senshinElementDamageOptions,
      'ツメ - 炎属性ダメージ': senshinElementDamageOptions,
      'ツメ - 氷属性ダメージ': senshinElementDamageOptions,
      'ツメ - 風属性ダメージ': senshinElementDamageOptions,
      'ツメ - 雷属性ダメージ': senshinElementDamageOptions,
      'ツメ - 闇属性ダメージ': senshinElementDamageOptions,
      'ツメ - 土属性ダメージ': senshinElementDamageOptions,
      'ツメ - 光属性ダメージ': senshinElementDamageOptions,
      '扇 - 炎属性ダメージ': senshinElementDamageOptions,
      '扇 - 氷属性ダメージ': senshinElementDamageOptions,
      '扇 - 風属性ダメージ': senshinElementDamageOptions,
      '扇 - 雷属性ダメージ': senshinElementDamageOptions,
      '扇 - 闇属性ダメージ': senshinElementDamageOptions,
      '扇 - 土属性ダメージ': senshinElementDamageOptions,
      '扇 - 光属性ダメージ': senshinElementDamageOptions,
      'ムチ - 炎属性ダメージ': senshinElementDamageOptions,
      'ムチ - 氷属性ダメージ': senshinElementDamageOptions,
      'ムチ - 風属性ダメージ': senshinElementDamageOptions,
      'ムチ - 雷属性ダメージ': senshinElementDamageOptions,
      'ムチ - 闇属性ダメージ': senshinElementDamageOptions,
      'ムチ - 土属性ダメージ': senshinElementDamageOptions,
      'ムチ - 光属性ダメージ': senshinElementDamageOptions,
      'ブーメラン - 炎属性ダメージ': senshinElementDamageOptions,
      'ブーメラン - 氷属性ダメージ': senshinElementDamageOptions,
      'ブーメラン - 風属性ダメージ': senshinElementDamageOptions,
      'ブーメラン - 雷属性ダメージ': senshinElementDamageOptions,
      'ブーメラン - 闇属性ダメージ': senshinElementDamageOptions,
      'ブーメラン - 土属性ダメージ': senshinElementDamageOptions,
      'ブーメラン - 光属性ダメージ': senshinElementDamageOptions,
      '弓 - 炎属性ダメージ': senshinElementDamageOptions,
      '弓 - 氷属性ダメージ': senshinElementDamageOptions,
      '弓 - 風属性ダメージ': senshinElementDamageOptions,
      '弓 - 雷属性ダメージ': senshinElementDamageOptions,
      '弓 - 闇属性ダメージ': senshinElementDamageOptions,
      '弓 - 土属性ダメージ': senshinElementDamageOptions,
      '弓 - 光属性ダメージ': senshinElementDamageOptions,
      '鎌 - 炎属性ダメージ': senshinElementDamageOptions,
      '鎌 - 氷属性ダメージ': senshinElementDamageOptions,
      '鎌 - 風属性ダメージ': senshinElementDamageOptions,
      '鎌 - 雷属性ダメージ': senshinElementDamageOptions,
      '鎌 - 闇属性ダメージ': senshinElementDamageOptions,
      '鎌 - 土属性ダメージ': senshinElementDamageOptions,
      '鎌 - 光属性ダメージ': senshinElementDamageOptions,
      'ドラゴン系にダメージ': senshinTypeDamageOptions,
      'ムシ系にダメージ': senshinTypeDamageOptions,
      '獣系にダメージ': senshinTypeDamageOptions,
      'ゾンビ系にダメージ': senshinTypeDamageOptions,
      '植物系にダメージ': senshinTypeDamageOptions,
      '怪人系にダメージ': senshinTypeDamageOptions,
      '悪魔系にダメージ': senshinTypeDamageOptions,
      'エレメント系にダメージ': senshinTypeDamageOptions,
      '鳥系にダメージ': senshinTypeDamageOptions,
      '物質系にダメージ': senshinTypeDamageOptions,
      'マシン系にダメージ': senshinTypeDamageOptions,
      'スライム系にダメージ': senshinTypeDamageOptions,
      '水系にダメージ': senshinTypeDamageOptions,
      '開戦時 - 盾ガード率': [
        {
          'value': '+7%',
          'text': '+7%'
        },
        {
          'value': '+8%',
          'text': '+8%'
        },
        {
          'value': '+9%',
          'text': '+9%'
        },
        {
          'value': '+10%',
          'text': '+10%'
        }
      ],
      '開戦時 - マホトラのころも': [
        {
          'value': '+14%',
          'text': '+14%'
        },
        {
          'value': '+16%',
          'text': '+16%'
        },
        {
          'value': '+18%',
          'text': '+18%'
        },
        {
          'value': '+20%',
          'text': '+20%'
        }
      ],
      '開戦時 - 天使の守り': [
        {
          'value': '+7%',
          'text': '+7%'
        },
        {
          'value': '+8%',
          'text': '+8%'
        },
        {
          'value': '+9%',
          'text': '+9%'
        },
        {
          'value': '+10%',
          'text': '+10%'
        }
      ],
      '開戦時 - キラキラポーン': [
        {
          'value': '+7%',
          'text': '+7%'
        },
        {
          'value': '+8%',
          'text': '+8%'
        },
        {
          'value': '+9%',
          'text': '+9%'
        },
        {
          'value': '+10%',
          'text': '+10%'
        }
      ],
      '開戦時 - 聖女の守り': [
        {
          'value': '+7%',
          'text': '+7%'
        },
        {
          'value': '+8%',
          'text': '+8%'
        },
        {
          'value': '+9%',
          'text': '+9%'
        },
        {
          'value': '+10%',
          'text': '+10%'
        }
      ],
      '開戦時 - ピオラ': [
        {
          'value': '+14%',
          'text': '+14%'
        },
        {
          'value': '+16%',
          'text': '+16%'
        },
        {
          'value': '+18%',
          'text': '+18%'
        },
        {
          'value': '+20%',
          'text': '+20%'
        }
      ],
      '開戦時 - マホターン': [
        {
          'value': '+7%',
          'text': '+7%'
        },
        {
          'value': '+8%',
          'text': '+8%'
        },
        {
          'value': '+9%',
          'text': '+9%'
        },
        {
          'value': '+10%',
          'text': '+10%'
        }
      ]
    },
    '輝石のベルト': {
      'こうげき力': [
        {
          'value': '+6',
          'text': '+6'
        },
        {
          'value': '+8',
          'text': '+8'
        },
        {
          'value': '+10',
          'text': '+10'
        },
        {
          'value': '+12',
          'text': '+12'
        },
        {
          'value': '+14',
          'text': '+14'
        }
      ],
      'しゅび力': [
        {
          'value': '+10',
          'text': '+10'
        },
        {
          'value': '+12',
          'text': '+12'
        },
        {
          'value': '+14',
          'text': '+14'
        },
        {
          'value': '+16',
          'text': '+16'
        },
        {
          'value': '+18',
          'text': '+18'
        },
        {
          'value': '+22',
          'text': '+22'
        },
        {
          'value': '+24',
          'text': '+24'
        }
      ],
      'きようさ': [
        {
          'value': '+12',
          'text': '+12'
        },
        {
          'value': '+14',
          'text': '+14'
        },
        {
          'value': '+16',
          'text': '+16'
        },
        {
          'value': '+18',
          'text': '+18'
        },
        {
          'value': '+20',
          'text': '+20'
        },
        {
          'value': '+22',
          'text': '+22'
        },
        {
          'value': '+24',
          'text': '+24'
        }
      ],
      'すばやさ': [
        {
          'value': '+12',
          'text': '+12'
        },
        {
          'value': '+14',
          'text': '+14'
        },
        {
          'value': '+16',
          'text': '+16'
        },
        {
          'value': '+18',
          'text': '+18'
        },
        {
          'value': '+20',
          'text': '+20'
        },
        {
          'value': '+22',
          'text': '+22'
        },
        {
          'value': '+24',
          'text': '+24'
        }
      ],
      'おしゃれさ': [
        {
          'value': '+8',
          'text': '+8'
        },
        {
          'value': '+10',
          'text': '+10'
        },
        {
          'value': '+12',
          'text': '+12'
        },
        {
          'value': '+14',
          'text': '+14'
        },
        {
          'value': '+16',
          'text': '+16'
        }
      ],
      'こうげき魔力': [
        {
          'value': '+12',
          'text': '+12'
        },
        {
          'value': '+14',
          'text': '+14'
        },
        {
          'value': '+16',
          'text': '+16'
        },
        {
          'value': '+18',
          'text': '+18'
        },
        {
          'value': '+20',
          'text': '+20'
        },
        {
          'value': '+22',
          'text': '+22'
        },
        {
          'value': '+24',
          'text': '+24'
        },
        {
          'value': '+26',
          'text': '+26'
        }
      ],
      'かいふく魔力': [
        {
          'value': '+12',
          'text': '+12'
        },
        {
          'value': '+14',
          'text': '+14'
        },
        {
          'value': '+16',
          'text': '+16'
        },
        {
          'value': '+18',
          'text': '+18'
        },
        {
          'value': '+20',
          'text': '+20'
        },
        {
          'value': '+22',
          'text': '+22'
        },
        {
          'value': '+24',
          'text': '+24'
        },
        {
          'value': '+26',
          'text': '+26'
        }
      ],
      'さいだいHP': [
        {
          'value': '+6',
          'text': '+6'
        },
        {
          'value': '+8',
          'text': '+8'
        },
        {
          'value': '+10',
          'text': '+10'
        },
        {
          'value': '+12',
          'text': '+12'
        }
      ],
      'さいだいMP': [
        {
          'value': '+6',
          'text': '+6'
        },
        {
          'value': '+8',
          'text': '+8'
        },
        {
          'value': '+10',
          'text': '+10'
        },
        {
          'value': '+12',
          'text': '+12'
        }
      ],
      '移動速度': [
        {
          'value': '+1.0%',
          'text': '+1.0%'
        },
        {
          'value': '+2.0%',
          'text': '+2.0%'
        },
        {
          'value': '+3.0%',
          'text': '+3.0%'
        }
      ],
      'かいしん率': [
        {
          'value': '+0.5%',
          'text': '+0.5%'
        },
        {
          'value': '+1.0%',
          'text': '+1.0%'
        },
        {
          'value': '1.5%',
          'text': '1.5%'
        },
        {
          'value': '+2.0%',
          'text': '+2.0%'
        }
      ],
      '呪文発動速度': [
        {
          'value': '+0.5%',
          'text': '+0.5%'
        },
        {
          'value': '+1.0%',
          'text': '+1.0%'
        },
        {
          'value': '+1.5%',
          'text': '+1.5%'
        },
        {
          'value': '+2.0%',
          'text': '+2.0%'
        },
        {
          'value': '+2.5%',
          'text': '+2.5%'
        }
      ],
      '呪文ぼうそう率': [
        {
          'value': '+1.0%',
          'text': '+1.0%'
        },
        {
          'value': '+1.5%',
          'text': '+1.5%'
        },
        {
          'value': '+2.0%',
          'text': '+2.0%'
        },
        {
          'value': '+2.5%',
          'text': '+2.5%'
        },
        {
          'value': '+3.0%',
          'text': '+3.0%'
        }
      ],
      'MP消費しない率': [
        {
          'value': '+1%',
          'text': '+1%'
        },
        {
          'value': '+2%',
          'text': '+2%'
        },
        {
          'value': '+3%',
          'text': '+3%'
        },
        {
          'value': '+4%',
          'text': '+4%'
        },
        {
          'value': '+5%',
          'text': '+5%'
        }
      ],
      '通常ドロップ率': [
        {
          'value': '+1.1倍',
          'text': '+1.1倍'
        }
      ],
      'レアドロップ率': [
        {
          'value': '+1.1倍',
          'text': '+1.1倍'
        }
      ],
      '確率でダメージ10%反射': [
        {
          'value': '+6.0%',
          'text': '+6.0%'
        },
        {
          'value': '+7.0%',
          'text': '+7.0%'
        },
        {
          'value': '+8.0%',
          'text': '+8.0%'
        },
        {
          'value': '+9.0%',
          'text': '+9.0%'
        },
        {
          'value': '+10.0%',
          'text': '+10.0%'
        },
        {
          'value': '+14.0%',
          'text': '+14.0%'
        }
      ],
      '戦闘勝利時にHP回復': [
        {
          'value': '10～20',
          'text': '10～20'
        }
      ],
      '戦闘勝利時にMP回復': [
        {
          'value': '1～3',
          'text': '1～3'
        }
      ],
      '各弱体系耐性': [
        {
          'value': '+6.0%',
          'text': '+6.0%'
        },
        {
          'value': '+8.0%',
          'text': '+8.0%'
        },
        {
          'value': '+10.0%',
          'text': '+10.0%'
        }
      ],
      '眠りガード': resistanceOptions,
      'マヒガード': resistanceOptions,
      '混乱ガード': resistanceOptions,
      '封印ガード': resistanceOptions,
      '幻惑ガード': resistanceOptions,
      '呪いガード': resistanceOptions,
      '即死ガード': resistanceOptions,
      'どくガード': resistanceOptions,
      '魅了ガード': resistanceOptions,
      'MP吸収率ガード': resistanceOptions,
      'おびえガード': resistanceOptions,
      '転びガード': resistanceOptions,
      'しばりガード': resistanceOptions,
      '踊らされガード': resistanceOptions,
      'ふっとびガード': resistanceOptions,
      '炎の特技の攻撃ダメージ': kisekiElementDamageOptions,
      '氷の特技の攻撃ダメージ': kisekiElementDamageOptions,
      '風の特技の攻撃ダメージ': kisekiElementDamageOptions,
      '雷の特技の攻撃ダメージ': kisekiElementDamageOptions,
      '土の特技の攻撃ダメージ': kisekiElementDamageOptions,
      '闇の特技の攻撃ダメージ': kisekiElementDamageOptions,
      '光の特技の攻撃ダメージ': kisekiElementDamageOptions,
      '炎の呪文の攻撃ダメージ': kisekiElementDamageOptions,
      '氷の呪文の攻撃ダメージ': kisekiElementDamageOptions,
      '風の呪文の攻撃ダメージ': kisekiElementDamageOptions,
      '闇の呪文の攻撃ダメージ': kisekiElementDamageOptions,
      '光の呪文の攻撃ダメージ': kisekiElementDamageOptions,
      'ドラゴン系にダメージ': kisekiTypeDamageOptions,
      'ムシ系にダメージ': kisekiTypeDamageOptions,
      '獣系にダメージ': kisekiTypeDamageOptions,
      'ゾンビ系にダメージ': kisekiTypeDamageOptions,
      '植物系にダメージ': kisekiTypeDamageOptions,
      '怪人系にダメージ': kisekiTypeDamageOptions,
      '悪魔系にダメージ': kisekiTypeDamageOptions,
      'エレメント系にダメージ': kisekiTypeDamageOptions,
      '鳥系にダメージ': kisekiTypeDamageOptions,
      '物質系にダメージ': kisekiTypeDamageOptions,
      'マシン系にダメージ': kisekiTypeDamageOptions,
      'スライム系にダメージ': kisekiTypeDamageOptions,
      '水系にダメージ': kisekiTypeDamageOptions,
      '攻撃時 - どく': grantAbilityOptions,
      '攻撃時 - 眠り': grantAbilityOptions,
      '攻撃時 - マヒ': grantAbilityOptions,
      '攻撃時 - 混乱': grantAbilityOptions,
      '攻撃時 - 幻惑': grantAbilityOptions,
      '攻撃時 - 魅了': grantAbilityOptions,
      '攻撃時 - マホトーン': grantAbilityOptions,
      '攻撃時 - ヘナトス': grantAbilityOptions,
      '攻撃時 - ルカニ': grantAbilityOptions,
      '攻撃時 - リホ系解除': [
        {
          'value': '30%',
          'text': '30%'
        },
        {
          'value': '32%',
          'text': '32%'
        },
        {
          'value': '34%',
          'text': '34%'
        },
        {
          'value': '36%',
          'text': '36%'
        },
        {
          'value': '40%',
          'text': '40%'
        }
      ],
      '攻撃時 - マホ系解除': [
        {
          'value': '15%',
          'text': '15%'
        },
        {
          'value': '16%',
          'text': '16%'
        },
        {
          'value': '17%',
          'text': '17%'
        },
        {
          'value': '18%',
          'text': '18%'
        },
        {
          'value': '20%',
          'text': '20%'
        }
      ],
      '攻撃時 - テンション下げ': [
        {
          'value': '15%',
          'text': '15%'
        },
        {
          'value': '16%',
          'text': '16%'
        },
        {
          'value': '17%',
          'text': '17%'
        },
        {
          'value': '18%',
          'text': '18%'
        },
        {
          'value': '20%',
          'text': '20%'
        }
      ],
      '攻撃時 - ぶきみなひかり': grantAbilityOptions,
      '攻撃時 - 魔導の書': grantAbilityOptions,
      '攻撃時 - 猛毒': grantAbilityOptions,
      '開戦時 - 盾ガード率': [
        {
          'value': '6.0%',
          'text': '6.0%'
        },
        {
          'value': '7.0%',
          'text': '7.0%'
        },
        {
          'value': '8.0%',
          'text': '8.0%'
        },
        {
          'value': '9.0%',
          'text': '9.0%'
        },
        {
          'value': '10.0%',
          'text': '10.0%'
        },
        {
          'value': '11.0%',
          'text': '11.0%'
        },
        {
          'value': '12.0%',
          'text': '12.0%'
        }
      ],
      '開戦時 - かいしん率': [
        {
          'value': '6.0%',
          'text': '6.0%'
        },
        {
          'value': '7.0%',
          'text': '7.0%'
        },
        {
          'value': '8.0%',
          'text': '8.0%'
        },
        {
          'value': '9.0%',
          'text': '9.0%'
        },
        {
          'value': '10.0%',
          'text': '10.0%'
        },
        {
          'value': '11.0%',
          'text': '11.0%'
        },
        {
          'value': '12.0%',
          'text': '12.0%'
        }
      ],
      '開戦時 - ピオラ': [
        {
          'value': '12%',
          'text': '12%'
        },
        {
          'value': '14%',
          'text': '14%'
        },
        {
          'value': '16%',
          'text': '16%'
        },
        {
          'value': '18%',
          'text': '18%'
        },
        {
          'value': '20%',
          'text': '20%'
        },
        {
          'value': '24%',
          'text': '24%'
        }
      ],
      '開戦時 - マホキテ': [
        {
          'value': '12%',
          'text': '12%'
        },
        {
          'value': '14%',
          'text': '14%'
        },
        {
          'value': '16%',
          'text': '16%'
        },
        {
          'value': '18%',
          'text': '18%'
        },
        {
          'value': '20%',
          'text': '20%'
        },
        {
          'value': '24%',
          'text': '24%'
        }
      ],
      '開戦時 - メイクアップ': [
        {
          'value': '12%',
          'text': '12%'
        },
        {
          'value': '14%',
          'text': '14%'
        },
        {
          'value': '16%',
          'text': '16%'
        },
        {
          'value': '18%',
          'text': '18%'
        },
        {
          'value': '20%',
          'text': '20%'
        },
        {
          'value': '24%',
          'text': '24%'
        }
      ],
      '開戦時 - マホトラのころも': [
        {
          'value': '12%',
          'text': '12%'
        },
        {
          'value': '14%',
          'text': '14%'
        },
        {
          'value': '16%',
          'text': '16%'
        },
        {
          'value': '18%',
          'text': '18%'
        },
        {
          'value': '20%',
          'text': '20%'
        },
        {
          'value': '24%',
          'text': '24%'
        }
      ],
      '開戦時 - 天使の守り': [
        {
          'value': '6.0%',
          'text': '6.0%'
        },
        {
          'value': '7.0%',
          'text': '7.0%'
        },
        {
          'value': '8.0%',
          'text': '8.0%'
        },
        {
          'value': '9.0%',
          'text': '9.0%'
        },
        {
          'value': '10.0%',
          'text': '10.0%'
        },
        {
          'value': '11.0%',
          'text': '11.0%'
        },
        {
          'value': '12.0%',
          'text': '12.0%'
        }
      ],
      '開戦時 - キラキラポーン': [
        {
          'value': '6.0%',
          'text': '6.0%'
        },
        {
          'value': '7.0%',
          'text': '7.0%'
        },
        {
          'value': '8.0%',
          'text': '8.0%'
        },
        {
          'value': '9.0%',
          'text': '9.0%'
        },
        {
          'value': '10.0%',
          'text': '10.0%'
        },
        {
          'value': '12.0%',
          'text': '12.0%'
        }
      ],
      '開戦時 - 聖女の守り': [
        {
          'value': '6.0%',
          'text': '6.0%'
        },
        {
          'value': '7.0%',
          'text': '7.0%'
        },
        {
          'value': '8.0%',
          'text': '8.0%'
        },
        {
          'value': '9.0%',
          'text': '9.0%'
        },
        {
          'value': '10.0%',
          'text': '10.0%'
        },
        {
          'value': '11.0%',
          'text': '11.0%'
        },
        {
          'value': '12.0%',
          'text': '12.0%'
        }
      ],
      '開戦時 - ためる': [
        {
          'value': '6.0%',
          'text': '6.0%'
        },
        {
          'value': '7.0%',
          'text': '7.0%'
        },
        {
          'value': '8.0%',
          'text': '8.0%'
        },
        {
          'value': '9.0%',
          'text': '9.0%'
        },
        {
          'value': '10.0%',
          'text': '10.0%'
        },
        {
          'value': '12.0%',
          'text': '12.0%'
        }
      ],
      '開戦時 - 必殺チャージ': [
        {
          'value': '1.0%',
          'text': '1.0%'
        },
        {
          'value': '2.0%',
          'text': '2.0%'
        },
        {
          'value': '3.0%',
          'text': '3.0%'
        }
      ],
      '開戦時 - バイシオン': [
        {
          'value': '12%',
          'text': '12%'
        },
        {
          'value': '14%',
          'text': '14%'
        },
        {
          'value': '16%',
          'text': '16%'
        },
        {
          'value': '18%',
          'text': '18%'
        },
        {
          'value': '20%',
          'text': '20%'
        },
        {
          'value': '24%',
          'text': '24%'
        }
      ],
      '開戦時 - リホイミ': [
        {
          'value': '12%',
          'text': '12%'
        },
        {
          'value': '14%',
          'text': '14%'
        },
        {
          'value': '16%',
          'text': '16%'
        },
        {
          'value': '18%',
          'text': '18%'
        },
        {
          'value': '20%',
          'text': '20%'
        },
        {
          'value': '24%',
          'text': '24%'
        }
      ],
      '夜　開戦時 - ためる': [
        {
          'value': '6.0%',
          'text': '6.0%'
        },
        {
          'value': '7.0%',
          'text': '7.0%'
        },
        {
          'value': '8.0%',
          'text': '8.0%'
        },
        {
          'value': '9.0%',
          'text': '9.0%'
        },
        {
          'value': '10.0%',
          'text': '10.0%'
        }
      ]
    }
  }
}
