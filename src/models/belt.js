// ベルト
import { beltDB } from './datastore'

export default {
  methods: {
    // ベルト情報をNeDBから取得する
    findBeltByAbilities: function (selectedAbilities) {
      let conditions = {}
      // 検索条件を構築
      if (selectedAbilities.length > 0) {
        conditions['$and'] = selectedAbilities.map(function (selectedAbility) {
          return {'abilities.type': selectedAbility}
        })
      }

      // 検索処理実施
      return new Promise(function (resolve, reject) {
        beltDB.find(conditions).sort({ created_at: -1 }).exec((error, docs) => {
          if (error !== null) {
            // 失敗時
            reject(error)
          } else {
            // 成功時
            resolve(docs)
          }
        })
      })
    },
    // ベルトをNeDBに保存する
    insertBelt: function (query) {
      beltDB.insert(query, (error, newDoc) => {
        if (error !== null) {
          // 失敗時
          console.error(error)
        }
      })
    },
    // ベルトをNeDBから削除する
    removeBeltById: function (deleteBeltId) {
      const query = { _id: deleteBeltId }
      beltDB.remove(query, {}, (error, numOfDocs) => {
        if (error !== null) {
          // 失敗時
          console.error(error)
        }
      })
    }
  }
}
