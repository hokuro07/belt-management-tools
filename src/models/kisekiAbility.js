// 輝石ベルト効果
import { kisekiAbilityDB } from './datastore'

export default {
  methods: {
    // 封印情報をNeDBから取得する
    findKisekiAbility: function () {
      return new Promise(function (resolve, reject) {
        kisekiAbilityDB.find({}, { checked: 1 }).sort({ order: 1 }).exec((error, docs) => {
          if (error !== null) {
            // 失敗時
            reject(error)
          } else {
            // 成功時
            resolve(docs)
          }
        })
      })
    },
    // 輝石のベルト封印情報をNeDBに保存する
    insertKisekiAbility: function (query) {
      kisekiAbilityDB.insert(query, (error, newDoc) => {
        if (error !== null) {
          // 失敗時
          console.error(error)
        }
      })
    },
    // 輝石のベルト封印情報をNeDBから削除する
    removeKisekiAbility: function () {
      kisekiAbilityDB.remove({ }, { multi: true }, (error, numOfDocs) => {
        if (error !== null) {
          // 失敗時
          console.error(error)
        }
      })
    }
  }
}
