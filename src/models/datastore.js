import Datastore from 'nedb'
import path from 'path'
import { remote } from 'electron'

const beltDB = new Datastore({
  autoload: true,
  filename: path.join(remote.app.getPath('userData'), '/belt.db')
})

const senshinAbilityDB = new Datastore({
  autoload: true,
  filename: path.join(remote.app.getPath('userData'), '/senshinAbility.db')
})

const kisekiAbilityDB = new Datastore({
  autoload: true,
  filename: path.join(remote.app.getPath('userData'), '/kisekiAbility.db')
})

export {
  beltDB,
  senshinAbilityDB,
  kisekiAbilityDB
}
