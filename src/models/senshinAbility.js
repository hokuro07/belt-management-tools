// 戦神ベルト効果
import { senshinAbilityDB } from './datastore'

export default {
  methods: {
    // 封印情報をNeDBから取得する
    findSenshinAbility: function () {
      return new Promise(function (resolve, reject) {
        senshinAbilityDB.find({}, { checked: 1 }).sort({ order: 1 }).exec((error, docs) => {
          if (error !== null) {
            // 失敗時
            reject(error)
          } else {
            // 成功時
            resolve(docs)
          }
        })
      })
    },
    // 戦神のベルト封印情報をNeDBに保存する
    insertSenshinAbility: function (query) {
      senshinAbilityDB.insert(query, (error, newDoc) => {
        if (error !== null) {
          // 失敗時
          console.error(error)
        }
      })
    },
    // 戦神のベルト封印情報をNeDBから削除する
    removeSenshinAbility: function () {
      senshinAbilityDB.remove({ }, { multi: true }, (error, numOfDocs) => {
        if (error !== null) {
          // 失敗時
          console.error(error)
        }
      })
    }
  }
}
