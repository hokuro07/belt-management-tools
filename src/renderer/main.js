import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'

// bootstrap-vue
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// fontawesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// custom.scss
import './assets/scss/custom.scss'

// scroll library
import VScrollLock from 'v-scroll-lock'

// fontawesome アイコン読み込み
library.add(fas)

// fontawesome Vueコンポーネント作成
Vue.component('v-fa', FontAwesomeIcon)

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

// BootstrapVue
Vue.use(BootstrapVue)

// v-scrolle-lock
Vue.use(VScrollLock)

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  template: '<App/>'
}).$mount('#app')
