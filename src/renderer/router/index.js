import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = {
  routes: [
    {
      path: '/',
      name: 'belt-page',
      component: require('@/components/BeltPage').default
    },
    {
      path: '/senshin-seal',
      name: 'senshin-seal-page',
      component: require('@/components/SenshinSealPage').default
    },
    {
      path: '/kiseki-seal',
      name: 'kiseki-seal-page',
      component: require('@/components/KisekiSealPage').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
}

export default new Router(routes)
