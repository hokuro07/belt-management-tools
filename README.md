# DQ10ベルト管理ツール
DQ10の戦神のベルト、輝石のベルトの管理をサポートするアプリケーションです。

# 注意

本ツール使用で起きた問題についての責任は負いかねます。  
<span style="color: red; ">自己責任</span>で利用してください。


# ダウンロード
* [Windows - Ver 0.0.1](https://gitlab.com/hokuro07/belt-management-tools/-/jobs/artifacts/master/raw/build/DQ10ベルト管理ツール%20Setup%200.0.1.exe?job=build_win)

※ インストーラを提供しますが、動作を保証するものではありません。

# インストール
「WindowsによってPCが保護されました」と表示されると思いますが、PCに危害を及ぼす可能性がある場合に、PCを保護するために表示されます。  
安全なアプリケーションでも、発行元が不明な場合や実績の少ないアプリケーションの場合に表示されます。  

注意でも述べましたが、本ツールの利用は自己責任となります。  
不安な方は本ツールの利用をお控えください。

本ツールをインストールする場合は、「詳細情報」から「実行」を選択ください。  
インストールは、インストーラの指示に従って進めてください。  

# 使い方
## 最初に
戦神のベルト、輝石のベルトの効果をカスタマイズしてください。  
効果を登録することで、ベルト登録時に効果を選択することが可能です。  
この設定を行わないと効果の選択ができません。

![sample01](https://gitlab.com/hokuro07/belt-management-tools/uploads/479ce2dd842906d08db380ecfd01a8ea/sample01.gif)

## ベルトの登録
＋アイコンからベルトの登録をすることが可能です。  
付与効果で選択できる効果は「最初に」で登録した効果になります。 

![sample02](https://gitlab.com/hokuro07/belt-management-tools/uploads/58db8afb1d6e05835f45571bdcc693b5/sample02.gif)

## ベルトの検索
タグ付の要領でベルトの絞り込みを行うこと可能です。  
複数のタグをつけた場合、AND検索となります。  
(「こうげき魔力とかいふく魔力」と「こうげき力」を検索タグに指定した場合、2つの効果が付与されたベルトのみ表示されます)

![sample03](https://gitlab.com/hokuro07/belt-management-tools/uploads/2cd3d757cbe98c78a053158efa82ae2b/sample03.gif)

## ベルトの削除
ベルト横のゴミ箱アイコンからベルトの削除が可能です。

![sample04](https://gitlab.com/hokuro07/belt-management-tools/uploads/8feb17cdd3a03b82093790234c1aa2f4/sample04.gif)

# アンインストール
「コントロールパネル > プログラム > プログラムと機能」を表示し、DQ10ベルト管理ツールをアンインストールしてください。

# 利用した技術
* electron-vue
* BootstrapVue
* fontawesome

# ライセンス 
"DQ10ベルト管理ツール" は[MIT license](https://en.wikipedia.org/wiki/MIT_License)で配布しています。

---

This project was generated with [electron-vue](https://github.com/SimulatedGREG/electron-vue)@[45a3e22](https://github.com/SimulatedGREG/electron-vue/tree/45a3e224e7bb8fc71909021ccfdcfec0f461f634) using [vue-cli](https://github.com/vuejs/vue-cli). Documentation about the original structure can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/index.html).
